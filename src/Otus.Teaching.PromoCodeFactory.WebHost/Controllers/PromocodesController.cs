﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _repository;
        private readonly IPreferenceRepository _preferenceRepostory;
        private readonly IRepository<CustomerPreference> _cpRepository;
        public PromocodesController(IRepository<PromoCode> repository, IPreferenceRepository prefRep, 
            IRepository<CustomerPreference> custprefRep) 
        {
            _repository = repository;
            _preferenceRepostory = prefRep;
            _cpRepository = custprefRep;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {

            var pcodes =  await _repository.GetAllAsync();
            var res = pcodes.Select(rec => new PromoCodeShortResponse()
            {
                PartnerName = rec.PartnerName,
                ServiceInfo = rec.ServiceInfo,
                Code = rec.Code,
                Id = rec.Id,
                BeginDate = rec.BeginDate.ToString("dd.MM.yy HH:mm"),
                EndDate = rec.EndDate.ToString("dd.MM.yy HH:mm")
            }).ToList();
            return res;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            var pref = await _preferenceRepostory.GetByName(request.Preference);

            if (null == pref) return NotFound();

            if (0 == pref.Customers.Count) return NotFound();

            foreach (CustomerPreference cp in pref.Customers)
            {
                await _repository.Create(new PromoCode()
                {
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMonths(1),
                    Code = request.PromoCode,
                    Id = Guid.NewGuid(),
                    PreferenceId = pref.Id,
                    Preference = pref,
                    Customer = cp.Customer,
                });
            }
            return Ok();


        }
    }
}
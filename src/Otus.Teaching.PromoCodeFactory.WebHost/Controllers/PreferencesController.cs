﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private readonly IRepository<Preference> _prefRepository;

        public PreferencesController(IRepository<Preference> prefRepo)
        {
            _prefRepository = prefRepo;
        }


        /// <summary>
        ///  вернуть все доступные предпочтения 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponce>>> GetAllAsync()
        {
            var pref = await _prefRepository.GetAllAsync();
            var res = pref.Select(rec => new PreferenceResponce()
            {
                Id = rec.Id,
                Name = rec.Name
            });
            return Ok(res);
        }
    }
}

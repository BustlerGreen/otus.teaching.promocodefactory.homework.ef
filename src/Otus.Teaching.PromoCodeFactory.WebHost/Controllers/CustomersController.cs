﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
    
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerPreference> _custprefRepository;
        private readonly IRepository<Preference> _prefRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;

        public CustomersController(IRepository<Customer> repository, IRepository<CustomerPreference> custpref, 
            IRepository<Preference> pref, IRepository<PromoCode> promo )
        {
            _customerRepository = repository;
            _custprefRepository = custpref;
            _prefRepository = pref;
            _promocodeRepository = promo;
        }

        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Получить всех Customer в коротком формате
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = await _customerRepository.GetAllAsync();
            return customers.Select(rec =>
                new CustomerShortResponse()
                {
                    Id = rec.Id,
                    FirstName = rec.FirstName,
                    LastName = rec.LastName,
                    Email = rec.Email
                }
            ).ToList();
        }

        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Получить данные Cusstomer по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {

            var customer = await _customerRepository.GetByIdAsync(id);
            if (null == customer) return NotFound();
            
            var resp = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes.Select(rec => new PromoCodeShortResponse()
                {
                    Code = rec.Code,
                    ServiceInfo = rec.ServiceInfo,
                    BeginDate = rec.BeginDate.ToString("dd.MM.yy HH:mm"),
                    EndDate = rec.EndDate.ToString("dd.MM.yy HH:mm"),
                    PartnerName = rec.PartnerName,
                    Id = rec.Id
                }).ToList(),
                Preferences = customer.Preferences.Select(rec => new PreferenceResponce()
                { 
                    Id = rec.Preference.Id,
                    Name = rec.Preference.Name 
                }).ToList()
            };
            return Ok(resp);
        }

        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Создать Customer
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var custId = Guid.NewGuid();
            Customer cust = new Customer()
            {
                Id = custId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Preferences = request.PreferenceIds.Select(rec => new CustomerPreference()
                {
                    CustomerId = custId,
                    PreferenceId = rec
                }).ToList()
            };
            var res = await _customerRepository.Create(cust);

            if (null == res) return BadRequest(); 
            
            return Ok();
        }

        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// редактируем данные Customer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var cust = await _customerRepository.GetByIdAsync(id);
            if (null == cust) return NotFound();

            await _custprefRepository.DeleteRangeAsync(cust.Preferences.Select(rec => rec.Id).ToList());

            var prefs = null == request.PreferenceIds ? new List<Preference>() : await _prefRepository.GetRangeAsync(request.PreferenceIds);
            cust.FirstName = request.FirstName;
            cust.LastName = request.LastName;
            cust.Email = request.Email;
            cust.Preferences = prefs.Select(rec => new CustomerPreference()
            {
                CustomerId = id,
                PreferenceId = rec.Id
            }).ToList();
            await _customerRepository.Update(cust);
            return Ok();
        }

        //------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Delete Customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (null == customer) return NotFound();

            var pcodes = customer.PromoCodes.Select(rec => rec.Id).ToList();
            
            if (0 < pcodes.Count) await _promocodeRepository.DeleteRangeAsync(pcodes);
            
            await _custprefRepository.DeleteRangeAsync(customer.Preferences.Select(rec => rec.Id).ToList());

            await _customerRepository.DeleteAsync(id);
            
            return Ok();
        }
    }
}
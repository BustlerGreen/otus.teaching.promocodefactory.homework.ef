﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeShortResponse
    {
        public Guid Id { get; set; }
        [MaxLength(30)]
        public string FullName { get; set; }
        [MaxLength(128)]
        public string Email { get; set; }
    }
}
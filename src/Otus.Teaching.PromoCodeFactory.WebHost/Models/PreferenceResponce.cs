﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponce
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

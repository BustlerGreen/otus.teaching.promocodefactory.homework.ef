﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetRangeAsync(IEnumerable<Guid> ids);
        
        Task<T> GetByIdAsync(Guid id);

        Task<T> DeleteAsync(Guid id);

        Task<IEnumerable<T>> DeleteRangeAsync(IEnumerable<Guid> ids);

        Task<T> Create(T entity);

        Task<T> Update(T entity);
        
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(96)]
        public string Name { get; set; }
        [MaxLength(256)]
        public string Description { get; set; }
        public virtual List<Employee> Employees { get; set; } = new List<Employee>();
      
    }
}
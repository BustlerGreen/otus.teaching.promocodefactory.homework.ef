﻿using System;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(128)]
        public string Code { get; set; }
        [MaxLength(128)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }
        [MaxLength(128)]
        public string PartnerName { get; set; }

        public Guid  PreferenceId { get; set; }
        public Preference Preference { get; set; }

        //public List<CustPromoCode> Customers { get; set; } = new List<CustPromoCode>();

        public Customer Customer { get; set; }
    }
}
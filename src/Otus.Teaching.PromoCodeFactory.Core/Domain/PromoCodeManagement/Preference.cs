﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseEntity
    {
        [MaxLength(128)]
        public string Name { get; set; }

        public List<CustomerPreference> Customers { get; set; } = new List<CustomerPreference>();
    }
}
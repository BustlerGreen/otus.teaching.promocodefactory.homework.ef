﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(40)]
        public string LastName { get; set; }
        [MaxLength(70)]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(128)]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 

        //public List<CustPromoCode> PromoCodes { get; set; }
        public List<PromoCode> PromoCodes { get; set; }

        public List<CustomerPreference> Preferences { get; set; }

        public string DUMB { get; set; } // migrate

        public Customer()
        {
            PromoCodes = new List<PromoCode>();
            Preferences = new List<CustomerPreference>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T: BaseEntity
    {
        private readonly AppDataContext _dataContext;

        //--------------------------------------------------------------------------------------
        public EfRepository(AppDataContext dbcontext)
        {
            _dataContext = dbcontext;
        }

        //--------------------------------------------------------------------------------------
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();
        }

        //--------------------------------------------------------------------------------------
        public async Task<IEnumerable<T>> GetRangeAsync(IEnumerable<Guid> ids)
        {
            return await _dataContext.Set<T>().Where( rec => ids.Contains(rec.Id)).ToListAsync();
        }

        //--------------------------------------------------------------------------------------
        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(rec => rec.Id == id);
        }


        //--------------------------------------------------------------------------------------
        public async Task<T> DeleteAsync(Guid id)
        {
            var res = await _dataContext.Set<T>().FirstOrDefaultAsync(rec => rec.Id == id);
            if (null != res) _dataContext.Set<T>().Remove(res);
            await _dataContext.SaveChangesAsync();
            return res;
        }

        public async Task<IEnumerable<T>> DeleteRangeAsync(IEnumerable<Guid> ids)
        {
            var dc = _dataContext.Set<T>();
            var tDel = await dc.Where(rec => ids.Contains(rec.Id)).ToListAsync();
            dc.RemoveRange(tDel);
            await _dataContext.SaveChangesAsync();
            return tDel;
        }


        //--------------------------------------------------------------------------------------
        public async Task<T> Create(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

        //--------------------------------------------------------------------------------------
        public async Task<T> Update(T entity)
        {
            var res = await _dataContext.Set<T>().FirstOrDefaultAsync(rec => rec.Id == entity.Id);
            if (null == res) return null;
            _dataContext.Set<T>().Update(entity);
            await _dataContext.SaveChangesAsync();
            return entity;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public  class PreferenceRepository : EfRepository<Preference>, IPreferenceRepository
    {
        private readonly AppDataContext _dbcontext;

        public PreferenceRepository(AppDataContext context) : base(context)
        {
            _dbcontext = context;
        }

        public async Task<Preference> GetByName(string name)
        {
            var res = await _dbcontext.Set<Preference>()
                .Include(cs=>cs.Customers)
                .ThenInclude(c=>c.Customer)
                .FirstOrDefaultAsync(pc => pc.Name == name);
            return res;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>
    {
        private readonly AppDataContext _dataContext;

        public CustomerRepository(AppDataContext dbcontext):base(dbcontext)
        {
            _dataContext = dbcontext;
        }

        //-----------------------------------------------------------------------------------------
        public override async Task<IEnumerable<Customer>> GetAllAsync()
        {
            var res = await _dataContext.Set<Customer>()
                .Include(p => p.Preferences)
                .ThenInclude(p => p.Preference)
                .Include(p => p.PromoCodes)
                .ToListAsync();
            return res;
        }

        //-----------------------------------------------------------------------------------------
        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var res = await _dataContext.Set<Customer>()
                .Include(p => p.Preferences)
                .ThenInclude(p=>p.Preference)
                .Include(p => p.PromoCodes)
                .FirstOrDefaultAsync(rec=>rec.Id == id);
            return res;
        }
        
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Data;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    //=====================================================================================================
    public class AppDataContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        public DbSet<Customer> Customers { get; set; } = null!;
        public DbSet<CustomerPreference> CustomerPreferences { get; set; } = null!;
        public DbSet<Preference> Preferences { get; set; } = null!;
        public DbSet<PromoCode> PromoCodes { get; set; } = null;


        //-------------------------------------------------------------------------------------------
        public AppDataContext(DbContextOptions<AppDataContext> options) : base(options)
        {

        }

        //-------------------------------------------------------------------------------------------
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .HasKey(re => re.Id);
            modelBuilder.Entity<Employee>()
                .HasOne(em => em.Role)
                .WithMany(r => r.Employees)
                .HasForeignKey(em => em.RoleId);
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(p => p.Preferences)
                .HasForeignKey(cp => cp.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany(c => c.Customers)
                .HasForeignKey(cp => cp.PreferenceId);
        }

    }

    //=============================================================================================
    public interface IAppDbInitializer
    {
        //-------------------------------------------------------------------------------------------
        void InitDb();
    }

    //=============================================================================================
    public class AppDbInitializer : IAppDbInitializer
    {
        private readonly AppDataContext _dbContext;

        //-------------------------------------------------------------------------------------------
        public AppDbInitializer(AppDataContext context)
        {
            _dbContext = context;
        }


        //-------------------------------------------------------------------------------------------
        public void InitDb()
        {

            /*
            _dbContext.Database.EnsureDeleted();
            //_dbContext.Database.EnsureCreated();
            _dbContext.Database.Migrate();
            
            */
            /*
            _dbContext.Roles.AddRange(FakeDataFactory.Roles);

            _dbContext.Employees.AddRange(FakeDataFactory.Employees);

            _dbContext.Preferences.AddRange(FakeDataFactory.Preferences);

            _dbContext.Customers.AddRange(FakeDataFactory.Customers);


            _dbContext.SaveChanges();
            */
        }

    }

    //=====================================================================================================
    public class AppDbContextFactory : IDesignTimeDbContextFactory<AppDataContext>
    {
        public AppDataContext CreateDbContext(string[] args)
        {
            var OptionsBuilder = new DbContextOptionsBuilder<AppDataContext>();
            OptionsBuilder.UseSqlite("Data Source=F:\\PROJECTS\\otusPromoHW\\Ef\\promo.sq");
            return new AppDataContext(OptionsBuilder.Options);
        }
    }
}





